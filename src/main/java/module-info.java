module imp3d.selection {
	exports de.grogra.imp3d.selection;
	exports de.grogra.imp3d.aggregation;
	
	requires graph;
	requires imp;
	requires imp3d;
	requires platform;
	requires platform.core;
	requires platform.swing;
	requires utilities;
	requires xl.impl;
	requires rgg;
	requires math;
	requires vecmath;
	requires xl.core;
	
	requires java.desktop;
	
}