package de.grogra.imp3d.aggregation;

import de.grogra.graph.GraphState;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.edit.GraphSelectionImpl;
import de.grogra.pf.ui.edit.Selectable;
import de.grogra.pf.ui.edit.Selection;

/**
 * An aggregation Node is a simple node. It represents a aggregation of nodes. 
 * It should NOT have a parent other that another aggregation node. 
 * Its children are part of an aggregation. It is linked to its children with refinement edges.
 */
public class AggregationNode extends de.grogra.graph.impl.Node implements Selectable {
	
	public AggregationNode () {
		super(); 
	}

	/**
	 * Selectable is Required to skip the attribute editor of the reference
	 */
	@Override
	public Selection toSelection(Context ctx) {
		return new GraphSelectionImpl (ctx, GraphState.get (ctx.getWorkbench ()
				.getRegistry ().getProjectGraph (), UI.getThreadContext (ctx)),
				this, true);
	}

//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new AggregationNode ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new AggregationNode ();
	}

//enh:end
}
