package de.grogra.imp3d.aggregation;

import java.util.List;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.selection.Selection;
import de.grogra.imp3d.selection.SelectionTools;
import de.grogra.imp3d.selection.impl.NTypeSelection;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Context;

public class Commands {
	
	private static void registerAggregationNode(AggregationNode agg, Context ctx) {
		Registry r = ctx.getWorkbench().getRegistry();
		AggregationNodeRef ref = new AggregationNodeRef (agg);
		Item dir = r.getDirectory("/project/objects/aggregations", null);
		dir.addUserItemWithUniqueName(ref, "AggNode: "+Long.toString( agg.getId()));
	}

	public static void aggregateSelected (Item item, Object info, Context ctx)
	{
		List<Node> nodes = SelectionTools.getSelectedNodes(ctx);
		if (nodes==null) {
			return;
		}
		AggregationNode agg = new AggregationNode();
		for (Node n : nodes) {
			agg.addEdgeBitsTo (n, de.grogra.graph.Graph.REFINEMENT_EDGE, null);
		}
		registerAggregationNode(agg,ctx);
	}
	
	public static void aggregateAggregations (Item item, Object info, Context ctx)
	{
		Selection sel = new NTypeSelection(AggregationNode.$TYPE);
		List<Node> nodes = SelectionTools.doSelect(sel, ctx);
		AggregationNode agg = new AggregationNode();
		for (Node n : nodes) {
			agg.addEdgeBitsTo (n, de.grogra.graph.Graph.REFINEMENT_EDGE, null);
		}
		registerAggregationNode(agg,ctx);
	}

}
