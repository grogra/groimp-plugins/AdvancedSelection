package de.grogra.imp3d.selection.navigation;

import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;

import de.grogra.graph.GraphUtils;
import de.grogra.imp.ViewEventHandler;
import de.grogra.imp.edit.ViewSelection;
import de.grogra.imp.edit.ViewSelection.Entry;

public class MouseWheelSelection {

	private Entry[] mouseOverEntries;
	private Entry[] selectedEntries;
	private List<Entry> storedEntries;
	private Entry currentEntry;
	private ViewEventHandler vHandler;
	private ViewSelection vSelection;
	
	/* Entry[] with path (equal to nodes) and 
	 * value (property for mouse-over, mouse-over-selected or/and selected). 
	 * also manages highlighting 
	 * */
	//private static ViewSelection vSelection;
	
	// main processing function 
	MouseWheelSelection (ViewEventHandler vHandler) {
		this.vHandler = vHandler;
		this.vHandler.updateHighlight();
		this.vSelection = ViewSelection.get(this.vHandler.getView());
		this.storedEntries = new ArrayList<>();
		updateEntries();
	}
	
	private void unselectAllEntries () {
		for(int i = 0; i < this.selectedEntries.length; i++) {
			this.vSelection.removeAndAdd(ViewSelection.SELECTED,ViewSelection.MOUSE_OVER, selectedEntries[i].getPath());
		}
	}
	
	private void selectEntries () {
		for(int i = 0; i < this.selectedEntries.length; i++) {
			this.vSelection.add(ViewSelection.SELECTED, this.selectedEntries[i].getPath());
		}
	}
	
	private void selectStoredEntries () {
		if(this.storedEntries == null) {
			return;
		}
		for(int i = 0; i < this.storedEntries.size(); i++) {
			this.vSelection.add(ViewSelection.SELECTED, this.storedEntries.get(i).getPath());
		}
	}
	
	void selectCurrentEntry () {
		if(currentEntry == null) {
			return;
		}
		this.vSelection.add(ViewSelection.SELECTED, currentEntry.getPath());
	}
	
	void unselectCurrentEntry () {
		if(currentEntry == null) {
			return;
		}
		this.vSelection.removeAndAdd(ViewSelection.SELECTED, ViewSelection.MOUSE_OVER, currentEntry.getPath());
	}
	
	void storeCurrentEntry () {
		if(currentEntry == null) {
			return;
		}
		this.storedEntries.add(this.currentEntry);
		selectCurrentEntry();
	}
	
	void discardCurrentEntry () {
		if(currentEntry == null) {
			return;
		}
		this.storedEntries.remove(this.currentEntry);
		unselectCurrentEntry();
	}
	
	void updateEntries () {
		this.selectedEntries = this.vSelection.getAll(ViewSelection.SELECTED);
		unselectAllEntries();
		this.mouseOverEntries = this.vSelection.getAll(ViewSelection.MOUSE_OVER);
		selectEntries();
	}
	
	//currentEntry is set to the previous or next Entry depending on the rotation of the mousewheel
	void selectNextEntry (MouseWheelEvent wheelEvent) {
		
		updateEntries();
		
		if(this.mouseOverEntries == null) {
			return;
		}
		else if(this.mouseOverEntries.length == 0) {
			return;
		}
		
		if(this.currentEntry == null || !isCurrentEntryInMouseOver()) {
			this.currentEntry = this.mouseOverEntries[0];
			selectCurrentEntry();
			return;
		}

		unselectCurrentEntry();
		if (isCurrentEntryInMouseOver()) {
			int index = getIndexOfCurrentEntryInMouseOver();
			if (index == -1) {
				return;
			}
			int moeLength = this.mouseOverEntries.length;
			int next = (index - wheelEvent.getWheelRotation() + moeLength)% moeLength; 
			this.currentEntry = this.mouseOverEntries[next];
		} else {
			this.currentEntry = this.mouseOverEntries[0];
		}
		
		
		selectCurrentEntry();
		List<Entry> storedEntries = this.storedEntries;
		selectStoredEntries();
		//TODO Selektion für selectedEntries funktioniert noch nicht!!
		//TODO die ganzen Funktionen müssen noch getestet und dokumentiert werden
	}
	
	private boolean isCurrentEntryInMouseOver () {
		for(int i = 0; i < this.mouseOverEntries.length; i++) {
			if (GraphUtils.equal(this.currentEntry.getPath(), this.mouseOverEntries[i].getPath())) {
				return true;
			}
		}
		return false;
	}
	
	private int getIndexOfCurrentEntryInMouseOver() {
		for(int i = 0; i < this.mouseOverEntries.length; i++) {
			if (GraphUtils.equal(this.currentEntry.getPath(), this.mouseOverEntries[i].getPath())) {	
				return i;
			}
		}
		return -1;
	}
}
