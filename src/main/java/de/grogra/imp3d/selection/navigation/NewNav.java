
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.imp3d.selection.navigation;

import java.util.EventObject;

import java.awt.event.*;
import de.grogra.imp.*;

public class NewNav implements de.grogra.util.DisposableEventListener
{
	protected final ViewEventHandler vHandler;
	protected int lastX, lastY;
	protected MouseWheelSelection mws;
	
	public NewNav (ViewEventHandler h, java.util.EventObject e)
	{
		vHandler = h;
		lastX = ((MouseEvent) e).getX ();
		lastY = ((MouseEvent) e).getY ();
		mws = new MouseWheelSelection(vHandler);
	}
	
	public void dispose () {
	}

	public void eventOccured (EventObject e)
	{
		if (e instanceof MouseWheelEvent) {
			mws.selectNextEntry((MouseWheelEvent)e);
			
		} else if (e instanceof MouseEvent){
			
			MouseEvent mouseEvent = (MouseEvent) e;
			boolean isShiftDown = ((InputEvent) e).isShiftDown();
			int mouseEventButton = mouseEvent.getButton();
			int mouseEventID = mouseEvent.getID();
			
				if (mouseEventButton == MouseEvent.BUTTON1 && isShiftDown) {
						mws.selectCurrentEntry();
				}
				if (mouseEventButton == MouseEvent.BUTTON3 && isShiftDown) {
						mws.discardCurrentEntry();
				}
				if (mouseEventID == MouseEvent.MOUSE_RELEASED) {
					vHandler.disposeNavigator(null);
				}	
				if (mouseEventID == MouseEvent.MOUSE_MOVED) {
					vHandler.disposeNavigator(null);
				}
				if (mouseEventID ==	MouseEvent.MOUSE_DRAGGED) {
					vHandler.disposeNavigator(null);
				}
			}
	}
}
	