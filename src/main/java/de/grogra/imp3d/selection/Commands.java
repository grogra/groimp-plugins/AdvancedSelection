package de.grogra.imp3d.selection;

import java.util.List;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.aggregation.AggregationNode;
import de.grogra.imp3d.selection.dialogs.DialogTool;
import de.grogra.imp3d.selection.dialogs.InputDialogDegreeSelection;
import de.grogra.imp3d.selection.impl.AncestorSelection;
import de.grogra.imp3d.selection.impl.AngleSelection;
import de.grogra.imp3d.selection.impl.NTypeSelection;
import de.grogra.imp3d.selection.impl.ParentNTypeSelection;
import de.grogra.imp3d.selection.impl.AngleSelection.Angle;
import de.grogra.imp3d.selection.impl.VolumeSelection;
import de.grogra.imp3d.selection.impl.PlaneSelection;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;

/**
 * List of GroIMP commands that call all selections.
 * The commands are defined to be used from the GUI as menu items.
 */
public class Commands {
			
	/**
	 * Select all children of the given ancestor.
	 * 
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void selectByAncestor (Item item, Object info, Context context)
	{
		Node n = SelectionTools.getSelectedNode(context);
		Selection sel = new AncestorSelection(n);
		SelectionTools.select(SelectionTools.doSelect(sel, context), context);
	}
		

	
	/**
	 * This function selects nodes based on a span of degrees for as many graphs as selected 
	 * by at least a single node in in the corresponding graph. To use this function 
	 * you have to press the menu button in the viewer menu under Selection -> 'polarSelect'
	 * and fill out the pop up by setting start and end angle and an option to optionally
	 * include the stem, which represents all nodes that have no distance to the root as
	 * the root itself because it has a mathematical distance of zero.
	 */
	public static void selectDegreeInterval(Item item, Object info ,Context context)
	{	
		InputDialogDegreeSelection inputPanel = new InputDialogDegreeSelection();
		if (!inputPanel.getInput()) {
			return;
		}
		
		double startAngle = inputPanel.getStartAngle();
		double endAngle = inputPanel.getEndAngle();
		boolean includedStem = inputPanel.isStemIncluded();
		boolean xAxis = inputPanel.isXAxisSelected();
		
		Angle a = new Angle(startAngle, endAngle, xAxis);
		Selection sel = new AngleSelection(a, includedStem);
		SelectionTools.select(SelectionTools.doSelect(sel, context), context);
	}

	
	/**
	 * To use this function you have to insert a primitive volume object into the scene and have it selected after being
	 * rotated, translated or transformed in the the coordinate system. Then you can use it via the menu entry.
	 * The Function then selects every visible node in the primitive volume.
	 * 
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void selectByPrimitiveVolume(Item item, Object info, Context ctx)
	{	
		Node n = SelectionTools.getSelectedNode(ctx);
		
		if(n == null) {
			boolean exeption = DialogTool.handleException("errorPrimitive", "labelErrorPrimitive");
			return;
		}
		Selection sel = new VolumeSelection(n);
		SelectionTools.select(SelectionTools.doSelect(sel, ctx), ctx);
	}

	
	/**
	 * initialization method of the menu entry 
	 * 
	 * To use this function you have to insert a plane into the scene and have it selected after being
	 * rotated, translated or transformed in the the coordinate system. Then you can use it via the menu entry.
	 * The Function then selects every visible node over the selected plane.
	 * 
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void selectByPlaneOver(Item item, Object info, Context ctx)
	{
		Node n = SelectionTools.getSelectedNode(ctx);
		Selection sel = new PlaneSelection(n, true);
		SelectionTools.select(SelectionTools.doSelect(sel, ctx), ctx);
	}
	
	
	/**
	 * To use this function you have to insert a plane into the scene and have it selected after being
	 * rotated, translated or transformed in the the coordinate system. Then you can use it via the menu entry.
	 * The Function then selects every visible node under the selected plane.
	 *
	 * @param item The item that was clicked
	 * @param information Information about the clicked menu entry
	 * @param context The context where the button was clicked
	 */
	public static void selectByPlaneUnder(Item item, Object info, Context ctx)
	{
		Node n = SelectionTools.getSelectedNode(ctx);
		Selection sel = new PlaneSelection(n, false);
		SelectionTools.select(SelectionTools.doSelect(sel, ctx), ctx);
	}

	/**
	 * Select all nodes that are part of an aggregation
	 */
	public static void selectAggregationChildren (Item item, Object info, Context ctx)
	{
		Selection sel = new ParentNTypeSelection(AggregationNode.$TYPE);
		SelectionTools.select(SelectionTools.doSelect(sel, ctx), ctx);
	}
}
