//package de.grogra.imp3d.selection.impl;
//
//
//import de.grogra.graph.EdgePatternImpl;
//import de.grogra.graph.Graph;
//import de.grogra.graph.Path;
//import de.grogra.graph.VisitorImpl;
//import de.grogra.graph.impl.Edge;
//import de.grogra.graph.impl.Extent;
//import de.grogra.graph.impl.GraphManager;
//import de.grogra.graph.impl.Node;
//import de.grogra.imp3d.aggregation.AggregationDegreeNode;
//import de.grogra.imp3d.aggregation.AggregationNode;
//import de.grogra.imp3d.aggregation.AggregationPlaneNode;
//import de.grogra.imp3d.aggregation.AggregationVoxelNode;
//import de.grogra.imp3d.objects.ColoredNull;
//import de.grogra.imp3d.objects.ShadedNull;
//import de.grogra.imp3d.selection.Selection;
//import de.grogra.imp3d.selection.dialogs.DialogTool;
//import de.grogra.imp3d.selection.dialogs.InputDialogSectorDegree;
//import de.grogra.imp3d.selection.dialogs.InputDialogSectorPlane;
//import de.grogra.imp3d.selection.dialogs.InputDialogSectorVoxel;
//import de.grogra.pf.registry.Item;
//import de.grogra.pf.ui.Context;
//import de.grogra.rgg.Library;
//
//import java.util.ArrayList;
//import java.util.List;
//import javax.vecmath.Point3d;
//
//
//
//public class SectorSelection {
//	
//	public static void degreeSectorSelection (Item item, Object info, Context context){
//		
//		//user input for parameters
//		InputDialogSectorDegree input = new InputDialogSectorDegree();
//		boolean success = input.getInput();
//		if (!success) {
//			return;
//		}
//		int nSectors = input.getNSectors();
//		boolean xAxis = input.isXAxisSelected();
//		
//		Graph g = context.getWorkbench().getRegistry().getProjectGraph();
//		Node root = (Node) g.getRoot(Graph.MAIN_GRAPH);
//		
//		List<Node> aggregations = getAllAggregations(g);
//		deleteNodes(aggregations, context, root);
//		
//		addNodesToDegreeSector( g, nSectors ,xAxis);
//	}
//	
//	private static void addNodesToDegreeSector (Graph g, int nSectors, boolean xAxis) {
//		//TODO alle Aggregationen sollen vor der Erzeugung von Aggregationen gelöscht werden
//		//deleteAllAggregations();
//		
//		
//		List<AggregationDegreeNode> aggregations = new ArrayList<>();
//		AggregationDegreeNode aggDgrNodeZero = new AggregationDegreeNode(0,0, xAxis);
//		for (int i = 0; i < nSectors; i++) {
//			double degree = 360/nSectors;
//			double startAngle = degree*i;
//			double endAngle = degree*(i+1)-0.001;
//			AggregationDegreeNode aggDgrNode = new AggregationDegreeNode(startAngle, endAngle, xAxis );
//			aggregations.add(aggDgrNode);
//			//TODO: use a visitor
////			for(Edge node : g.getn) {
////				if(xAxis) {
////					Point3d point = Library.location((Node)node);
////					if ((point.y == 0) && (point.z == 0)) {
////						aggDgrNodeZero.addAggregatedNode((Node) node);
////						aggDgrNodeZero.linkNodes(aggDgrNodeZero,(Node)node, Graph.REFINEMENT_EDGE);
////						continue;
////					}
////				} else {
////					Point3d point = Library.location((Node)node);
////					if ((point.x == 0) && (point.y == 0)) {
////						aggDgrNodeZero.addAggregatedNode((Node) node);
////						aggDgrNodeZero.linkNodes(aggDgrNodeZero, (Node)node, Graph.REFINEMENT_EDGE);
////						continue;
////					}
////				}
////				boolean nodeIsInSector = Selection.isInAngle(node, startAngle, endAngle, false, xAxis);
////				if( nodeIsInSector ) {
////					aggDgrNode.addAggregatedNode((Node) node);
////					aggDgrNode.linkNodes(aggDgrNode, (Node)node, Graph.REFINEMENT_EDGE);
////				}
////			}
//		}
//	}
//	
//	public static void planeSectorSelection (Item item, Object info, Context context){
//		Graph g = context.getWorkbench().getRegistry().getProjectGraph();
//		Node root = (Node) g.getRoot(Graph.MAIN_GRAPH);
//		//user input for parameters
//		InputDialogSectorPlane input = new InputDialogSectorPlane();
//		boolean success = input.getInput();
//		if (!success) {
//			return;
//		}
//		double distance = input.getDistance();
//		double shift = input.getShift();
//		boolean xAxis = input.getxAxisSelected();
//		if ( distance <= 0 ){
//			DialogTool.handleException("errorNoValidInput", "labelNoValidInput");
//		}
//		
//		List<AggregationPlaneNode> aggregations = new ArrayList <>();
//		
//		List<Node> aggregationList = getAllAggregations(g);
//		deleteNodes(aggregationList, context, root);
////		for(Edge node : selectableNodes) {
////			addNodeToPlaneSector(aggregations, (Node) node, xAxis, distance, shift);
////		}		
//	}
//	
//	public static void voxelSectorSelection (Item item, Object info, Context context){
//		Graph g = context.getWorkbench().getRegistry().getProjectGraph();
//		Node root = (Node) g.getRoot(Graph.MAIN_GRAPH);
//		InputDialogSectorVoxel input = new InputDialogSectorVoxel();
//		boolean success = input.getInput();
//		if (!success) {
//			return;
//		}
//		double distance = input.getDistance();
//		if ( distance <= 0 ){
//			DialogTool.handleException("errorNoValidInput", "labelNoValidInput");
//		}
////		List<Edge> allNodes = SelectionTools.loadAllEdges(context);
////		List<Edge> selectableNodes = SelectionTools.filterSelectableEdges(allNodes);
//		List<AggregationVoxelNode> aggregations = new ArrayList <>();
//		
//		List<Node> aggregationList = getAllAggregations(g);
//		deleteNodes(aggregationList, context, root);
////		for(Edge edge : selectableNodes) {
////			addNodeToVoxelSector(aggregations,(Node) edge, distance);
////		}		
//	}
//	
//	private static void addNodeToPlaneSector
//		(List<AggregationPlaneNode> aggregations, Node node, boolean xAxis, double distance, double shift) {
//		
//			if(aggregations == null) {
//				createAggregationPlaneNode(aggregations, node, xAxis, distance, shift);
//			} else {	
//				boolean aggFound = searchAggregationPlaneAddNode (aggregations, node, xAxis);
//				if(!aggFound) {
//					createAggregationPlaneNode (aggregations,  node, xAxis, distance, shift);
//				}
//			}	
//	}
//	
//	private static boolean searchAggregationPlaneAddNode (List<AggregationPlaneNode> aggregations,Node node,boolean xAxis) {
//		Point3d point = Library.location(node);
//		if (xAxis) {
//			for (AggregationPlaneNode aggPlnNode: aggregations) {
//				double xMin = aggPlnNode.xMin;
//				double xMax = aggPlnNode.xMax;
//				if ((point.x >= xMin) && (point.x <= xMax)) {
//					aggPlnNode.addAggregatedNode(node);
//					aggPlnNode.linkNodes(aggPlnNode, node, Graph.REFINEMENT_EDGE);
//					return true;
//				}
//			}
//		} else {
//			for (AggregationPlaneNode aggPlnNode: aggregations) {
//				double zMin = aggPlnNode.zMin;
//				double zMax = aggPlnNode.zMax;
//				if ((point.z >= zMin) && (point.z <= zMax)) {
//					aggPlnNode.addAggregatedNode(node);
//					aggPlnNode.linkNodes(aggPlnNode, node, Graph.REFINEMENT_EDGE);
//					return true;
//				}
//			}
//		}
//		return false;
//	}
//	
//	private static void createAggregationPlaneNode 
//		(List<AggregationPlaneNode> aggregations, Node node, boolean xAxis, double distance, double shift) {
//		
//		Point3d point = Library.location(node);
//		if(xAxis) {
//			double xMin = (point.x % distance) * distance + shift;
//			double xMax = (point.x % distance +1) * distance + shift; 
//			AggregationPlaneNode aggPlnNode = new AggregationPlaneNode(xMin, xMax, xAxis);
//			aggPlnNode.addAggregatedNode(node);
//			aggPlnNode.linkNodes(aggPlnNode, node, Graph.REFINEMENT_EDGE);
//			aggregations.add(aggPlnNode);
//		} else {
//			double zMin = (point.z / distance) * distance + shift;
//			double zMax = (point.z / distance +1) * distance + shift; 
//			AggregationPlaneNode aggPlnNode = new AggregationPlaneNode(zMin, zMax, xAxis);
//			aggPlnNode.addAggregatedNode(node);
//			aggPlnNode.linkNodes(aggPlnNode, node, Graph.REFINEMENT_EDGE);
//			aggregations.add(aggPlnNode);
//		}
//	}
//	
//	private static void addNodeToVoxelSector 
//	(List<AggregationVoxelNode> aggregations, Node node, double distance) {
//		
//		if(aggregations == null) {
//			createAggregationVoxelNode (aggregations, node, distance);
//		} else {
//			boolean aggFound = searchAggregationVoxelAddNode(aggregations, node, distance);
//			if (!aggFound) {
//				createAggregationVoxelNode(aggregations, node, distance);
//			}
//		}
//	}
//	
//	private static void createAggregationVoxelNode 
//	(List<AggregationVoxelNode> aggregations, Node node, double distance) {
//		
//		Point3d point = Library.location(node);
//		double xMin = (point.x / distance) * distance;
//		double xMax = (point.x / distance +1) * distance;
//		double yMin = (point.y / distance) * distance;
//		double yMax = (point.y / distance +1) * distance;
//		double zMin = (point.z / distance) * distance;
//		double zMax = (point.z / distance +1) * distance;
//		AggregationVoxelNode aggVxlNode = new AggregationVoxelNode(xMin, xMax, yMin, yMax, zMin, zMax);
//		aggVxlNode.addAggregatedNode(node);
//		aggVxlNode.linkNodes(aggVxlNode, node, Graph.REFINEMENT_EDGE);
//		aggregations.add(aggVxlNode);
//	}
//	
//	private static boolean searchAggregationVoxelAddNode 
//		(List<AggregationVoxelNode> aggregations, Node node, double distance) {
//		
//		Point3d point = Library.location(node);
//		
//		for (AggregationVoxelNode aggVxlNode: aggregations) {
//			double xMin = aggVxlNode.xMin;
//			double xMax = aggVxlNode.xMax;
//			double yMin = aggVxlNode.yMin;
//			double yMax = aggVxlNode.yMax;
//			double zMin = aggVxlNode.zMin;
//			double zMax = aggVxlNode.zMax;
//			if ((point.x >= xMin) && (point.x <= xMax) && (point.y >= yMin) 
//					&& (point.y <= yMax) && (point.z >= zMin) && (point.z <= zMax)) {
//				aggVxlNode.addAggregatedNode(node);
//				aggVxlNode.linkNodes(aggVxlNode, node, Graph.REFINEMENT_EDGE);
//				return true;
//			}
//		}
//		return false;
//	}
//	
//	public static void aggregateAggregations (Item item, Object info, Context context) {
//		Graph g = context.getWorkbench().getRegistry().getProjectGraph();
//		Node root = (Node) g.getRoot(Graph.MAIN_GRAPH);
//		AggregationNode aggNode = new AggregationNode();
//		VisitorImpl v = new VisitorImpl ()
//        {
//            @Override
//            public Object visitEnter (Path path, boolean isNode)
//            {
//                Object nodeObject = path.getObject(-1);
//                if (!(nodeObject instanceof AggregationNode)) {
//                	return null;
//                }
//                AggregationNode node = (AggregationNode) nodeObject;
//        		Node parent = node.findAdjacent(false, true, Graph.REFINEMENT_EDGE); 
//        		if(parent != null) {
//        			return null;
//        		}
//                if (node.getId() != aggNode.getId()){
//                	aggNode.addAggregatedNode(node);
//                	aggNode.linkNodes(aggNode, node, Graph.REFINEMENT_EDGE);
//                }
//                return null;
//            }
//            @Override
//            public Object visitInstanceEnter ()
//            {
//                return STOP;
//            }
//        };
//        v.init (root.getCurrentGraphState (), EdgePatternImpl.TREE);
//        g.accept (root, v, null);
//	}
//	
//	public static void selectAggregationChildren (Item item, Object info, Context context) {
//		Graph g = context.getWorkbench().getRegistry().getProjectGraph();
//		Node root = (Node) g.getRoot(Graph.MAIN_GRAPH);
//		List<Edge> aggChildren = getAggregationChildren(root);
//		int size = aggChildren.size();
//		for (Edge child : aggChildren) {
//			System.err.println(child);
//		}
//		Selection.select(aggChildren, context);
//	}
//	
//	private static List<Edge> getAggregationChildren (Edge root) {
//		List<Edge> allChildren = new ArrayList<>();
//		VisitorImpl v = new VisitorImpl ()
//        {
//            @Override
//            public Object visitEnter (Path path, boolean isNode)
//            {	
//            	if (!(path.getObject(-1) instanceof ShadedNull || path.getObject(-1) instanceof ColoredNull)) {
//            		return null;
//            	}
//            	Node node = (Node) path.getObject(-1);
//            	Object parent = node.findAdjacent (true, false, Graph.REFINEMENT_EDGE);
//            	if(parent instanceof AggregationNode) {
//            		for(Edge child : allChildren) {
//            			if(((Node)child).getId() == node.getId()) {
//                			return null;
//            			}
//            		}
//            		allChildren.add(node);
//            	}
//                return null;
//            }
//            @Override
//            public Object visitInstanceEnter ()
//            {
//                return STOP;
//            }
//        };
//        v.init ( ((Node) root).getCurrentGraphState (), EdgePatternImpl.TREE);
//        ((Node)root).getGraph ().accept (root, v, null);
//        return allChildren;
//	}
//	
//	private static List<Node> getAllAggregations(Graph g) {
//		List<Node> a = new ArrayList<Node>();
//        Extent e = ((GraphManager)g).getExtent(AggregationNode.class);
//        for (int i = 0; i <= Node.LAST_EXTENT_INDEX; i++)
//		{
//			for (Node n = e.getFirstNode (i); n != null; n = e.getNextNode (n))
//			{
//				assert n.getGraph () != null : n;
//				a.add(n);
//			}
//		}
//        return a;
//	}
//	
//	private static void deleteNodes(List<Node> nodesToDelete, Context context, Node root) {
//		
//			/*
//			Transaction xa = node.getGraph().getActiveTransaction();
//    		Node parent = node.findAdjacent (true, false, -1);
//    		if (parent != null) {
//    			//(true if you add/remove a node)
//    			UI.executeLockedly (root.getGraph (), true, new Command ()
//    				{
//    					@Override
//    					public String getCommandName () {
//    						return null; // not needed
//    					}
//    					@Override
//    					public void run (Object info, Context c)
//    					{
//    						parent.getEdgeTo(node).remove(xa);
//    					}	
//    				}, null , context, JobManager.ACTION_FLAGS);         			
//    		}
//    		*/
//		
//		Node[] nodeArray = new Node[nodesToDelete.size()];
//		nodesToDelete.toArray(nodeArray);
//		context.getWorkbench().select(nodeArray);
//		context.getWorkbench().delete(nodeArray);
//	}
//	
//	protected void aggregateSelectedNodes (Item item, Object info, Context context ) {
//		AggregationNode aggNode = new AggregationNode();
//		Graph g = context.getWorkbench().getRegistry().getProjectGraph();
//		Node root = (Node) g.getRoot(Graph.MAIN_GRAPH);
//		
//		VisitorImpl v = new VisitorImpl ()
//        {
//            @Override
//            public Object visitEnter (Path path, boolean isNode)
//            {
//                if (Library.isSelected((Node)path.getObject(-1)) ) {
//                	Node node = (Node) path.getObject(-1);
//                	aggNode.addAggregatedNode(node);
//                	aggNode.linkNodes(aggNode, node, Graph.REFINEMENT_EDGE);
//                }
//                return null;
//            }
//
//            @Override
//            public Object visitInstanceEnter ()
//            {
//                return STOP;
//            }
//        };
//
//        v.init (root.getCurrentGraphState (), EdgePatternImpl.TREE);
//        g.accept (root, v, null);
//	}
//}
