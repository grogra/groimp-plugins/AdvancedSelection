package de.grogra.imp3d.selection.impl;

import javax.vecmath.Point3d;

import de.grogra.graph.impl.Node;
import de.grogra.imp3d.selection.Selection;
import de.grogra.rgg.Library;

public class AngleSelection extends Selection {
	
	
	public static class Angle{
		double startAngle;
		double endAngle;
		boolean usexAxis;
		
		public Angle(double startAngle, double endAngle, boolean useXAxis) {
			this.startAngle=startAngle;
			this.endAngle=endAngle;
			this.usexAxis=useXAxis;
		}
		
		public double getStartAngle() {
			return startAngle;
		}
		public double getEndAngle() {
			return endAngle;
		}
		public boolean getUsexAxis() {
			return usexAxis;
		}
	}
	
	private boolean includeStem;
	
	public AngleSelection(Object obj, boolean includeStem) {
		if (!(obj instanceof Angle)) {
			return;
		}
		this.obj=obj;
		this.includeStem=includeStem;
	}

	@Override
	protected boolean acceptImpl(Node node) {
		return isInAngle(node, (Angle)obj, includeStem);
	}
	
//	/**
//	 * checks if a node is in the interval for the individual graph
//	 * @param edge graph node
//	 * @param startAngle start of interval
//	 * @param endAngle end of interval
//	 * @param includeStem should nodes with radius from root be included?
//	 * @return return all nodes that are in an interval
//	 */
//	public static List<Edge> filterDegree(List<Edge> edges, double startAngle, 
//			double endAngle, boolean includeStem, boolean xAxis)
//	{
//		List<Edge> selected = new ArrayList<>();
//		for (Edge edge: edges) {
//			//sub-type check of the series of the generalization 
//			//Edge is super class of Null
//			if (!(edge instanceof Edge)) {
//				continue;
//			}
//			if (isInAngle(edge, startAngle, endAngle, includeStem, xAxis)) {
//				selected.add(edge);
//			}
//		}
//		return selected;
//	}

	
	
	/**
	 * this method checks for each node if it is the type of a Null object which have the attribute 
	 * for the translation on the x, y and z axes and initiates the actual method for the selection.
	 *  
	 * @param edges the list of all objects with the type of Edge
	 * @param startAngle the opening degree
	 * @param endAngle the closing degree
	 * @param includeStem a boolean if nodes of the distance 0 shall be included into the selection
	 * @return
	 */
	public boolean isInAngle(Node edge, Angle myAngle, boolean includeStem)
	{
		if (!(edge instanceof Node)) {
			return false;
		}
		Point3d point = Library.location((Node)edge);
		double angle;
		angle = de.grogra.imp3d.selection.Utils.radToDeg(Math.atan2(point.y, point.x));
			
		if (myAngle.getUsexAxis()) {
			if (includeStem && point.x == 0 && point.y == 0) {
				return true;
			} else if(!includeStem && point.x == 0 && point.y == 0) {
				return false;
			}
		}
		else {
			if (includeStem && point.x == 0 && point.z == 0) {
				return true;
			} else if(!includeStem && point.x == 0 && point.z == 0) {
				return false;
			}
		}
		if (angle < 0.0) {
			angle += 360.0; 
		}
		if ((myAngle.getStartAngle() <= angle)  && (angle  <= myAngle.getEndAngle())) {
			return true;
		}
		return false;
	}



}
