package de.grogra.imp3d.selection.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import de.grogra.imp3d.selection.Selection;
import de.grogra.util.I18NBundle;


public class InputDialogSectorDegree {

	//I18NBundle is used for translation purposes (see Ressources.properties)
	public static final I18NBundle I18N = I18NBundle.getInstance(Selection.class);
	//numbers of sectors
	static int nSectors;
	//on which axis should be selected
	boolean xAxisSelected;
	boolean zAxisSelected;
	
	//panel core
	JLabel label;
	JTextField nSectorField;
	JPanel panel;
	
	JRadioButton xButton;
	JRadioButton zButton;
	
	ButtonGroup group;
	String title;
	
	//fill panel
	public InputDialogSectorDegree() {	
			
		this.nSectors = -1;
		
		this.xAxisSelected = false;
		this.zAxisSelected = false;
		
		this.label = new JLabel (I18N.getString("degreeSectorLabel"));
		this.nSectorField = new JTextField(5);
		this.panel = new JPanel ();
		
		this.xButton = new JRadioButton(I18N.getString("x"));
		this.zButton = new JRadioButton(I18N.getString("z"));
		
		this.group = new ButtonGroup();
		this.group.add(this.xButton);
		this.group.add(this.zButton);
		this.title = I18N.getString("degreeSectorTitle");
		
		//---//
		
		this.panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(0,5,5,20);

		gbc.gridwidth=4;
		gbc.gridx=0;
		gbc.gridy=0;
		this.panel.add(this.label, gbc);
		
		gbc.gridwidth=4;
		gbc.gridx=0;
		gbc.gridy=1;
		this.panel.add(new JLabel(" "), gbc);
		
		gbc.gridwidth=2;
		gbc.gridx=0;
		gbc.gridy=2;
		this.panel.add(new JLabel(I18N.getString("degreeSectorCombobox")), gbc);
		
		gbc.gridwidth=1;
		gbc.gridx=3;
		gbc.gridy=2;
		this.panel.add(this.nSectorField, gbc);
		
		gbc.gridwidth=2;
		gbc.gridx=0;
		gbc.gridy=3;
		this.panel.add(new JLabel(I18N.getString("degreeSectorButtonGroup")), gbc);
		
		gbc.gridwidth=1;
		gbc.gridx=2;
		gbc.gridy=3;
		this.panel.add(this.xButton, gbc);
		
		gbc.gridwidth=1;
		gbc.gridx=3;
		gbc.gridy=3;
		this.panel.add(this.zButton, gbc);
	}	
		
	//store user's input into variables
	public boolean getInput() {
		
		boolean success = DialogTool.showInputDialog("inputTitle", panel);
		if(!success) {
			return success;
		}
		try {	
				this.nSectors = Integer.parseInt(this.nSectorField .getText());
				if(this.nSectors <= 0) {
					DialogTool.handleException("errorInvalidNumberValue", "labelInvalidNumberValue");
				}	
		}
		catch (NumberFormatException nfe) {
			return DialogTool.handleException("errorDataType", "labelErrorDataType");
		}
		if (xButton.isSelected()) {
			this.xAxisSelected = true;
		} else if (zButton.isSelected()) {
			this.zAxisSelected = true;		
		} else {
			return DialogTool.handleException("errorAxis", "labelErrorAxis");
		}
		return success;	
	}
	
	public boolean isXAxisSelected () {
		return xAxisSelected;
	}
	
	public boolean isZAxisSelected() {
		return zAxisSelected;
	}
	
	public int getNSectors() {
		return this.nSectors;
	}
}
