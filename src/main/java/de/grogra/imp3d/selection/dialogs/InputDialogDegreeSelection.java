package de.grogra.imp3d.selection.dialogs;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import de.grogra.imp3d.selection.Selection;
import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.I18NBundle;

/**
 * a gui that pops up after the polarSelect button is pressed to request parameters for the selection
 * @return an array of objects that contains three elements: 
 * a span of degree with start and end as a boolean to include nodes with a distance of zero
 */

public class InputDialogDegreeSelection {
	
	//I18NBundle is used for translation purposes (see Ressources.properties)
	public static final I18NBundle I18N = I18NBundle.getInstance(Selection.class);
	//Angles for an interval 
	double startAngle, endAngle;
	//should elements without a distance to the referencing point be included?
	boolean stemIncluded = false;
	//on which axis it should be selected
	boolean xAxisSelected = false, zAxisSelected = false;
	
	//panel core
	JPanel panel = new JPanel ();
	JTextField startAngleField = new JTextField(5);
	JTextField endAngleField = new JTextField(5);
	JCheckBox checkBox = new JCheckBox(I18N.getString("degreeSelectionCheckbox"));
	
	JRadioButton xButton = new JRadioButton(I18N.getString("x"));
	JRadioButton zButton = new JRadioButton(I18N.getString("z"));
	ButtonGroup group = new ButtonGroup();

	//fill panel
	public InputDialogDegreeSelection() {
		
		panel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(0,5,5,20);
		
		gbc.gridx=0;
		gbc.gridy=0;
		panel.add(new JLabel("start angle:"), gbc);
		
		gbc.gridx=1;
		gbc.gridy=0;
		panel.add(startAngleField, gbc);
		
		gbc.gridx=2;
		gbc.gridy=0;
		panel.add(new JLabel("end angle:"), gbc);
		
		gbc.gridx=3;
		gbc.gridy=0;
		panel.add(endAngleField, gbc);
		
		group.add(xButton);
		group.add(zButton);
		
		gbc.gridwidth=1;
		gbc.gridx=0;
		gbc.gridy=1;
		panel.add(xButton, gbc);
		
		gbc.gridwidth=1;
		gbc.gridx=1;
		gbc.gridy=1;
		panel.add(zButton, gbc);
		
		gbc.gridwidth=2;
		gbc.gridx=2;
		gbc.gridy=1;
		panel.add(checkBox, gbc);
	}	
	
	//store user's input into variables
	public boolean getInput() {
		
		boolean success = DialogTool.showInputDialog("inputTitle", panel);
		if(!success) {
			return success;
		}
		
		try {
				startAngle = Double.parseDouble(startAngleField.getText());
				endAngle = Double.parseDouble(endAngleField.getText());
				if ( this.startAngle <= 0 || this.endAngle <= 0 ) {
					DialogTool.handleException("errorInvalidNumberValue", "labelInvalidNumberValue");
				}
				
			} 
			catch (NumberFormatException nfe) {
					return DialogTool.handleException("errorNumber",I18N.getString("labelErrorNumber"));
			}
		
			//axis
			stemIncluded = checkBox.isSelected();
			
			if (xButton.isSelected()) {
				xAxisSelected = true;
			} else if (zButton.isSelected()) {
				zAxisSelected = true;
			} else {
				return DialogTool.handleException("errorAxis", I18N.getString("labelErrorAxis"));
			}
			
			return success;
	}
	
	//getters
	public double getStartAngle() {	
		return startAngle;
	}
	
	public double getEndAngle() {
		return endAngle;
	}
	
	public boolean isStemIncluded () {
		return stemIncluded;
	}
	
	public boolean isXAxisSelected () {
		return xAxisSelected;
	}
	
	public boolean isZAxisSelected() {
		return zAxisSelected;
	}
}

